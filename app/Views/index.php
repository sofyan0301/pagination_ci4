<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tes PT.Alumagubi</title>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/app.css'); ?>">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/table.css'); ?>">
</head>

<body>
    <h2 class="title">Data List</h2>
    <table>
        <thead>
            <th>Product name</th>
            <th>Category</th>
            <th>Price</th>
        </thead>
        <tbody>
            <?php
            foreach ($data_lists as $key => $data) { ?>
                <tr>
                    <td><?php echo $data['title']; ?></td>
                    <td><?php echo $data['name_category']; ?></td>
                    <td><?php echo $data['price']; ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <?= $pager->links() ?>
</body>

</html>