<?php

namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$model = new \App\Models\Product();
		$data = $model->getDataList();

		return view('index', $data);
	}
}
