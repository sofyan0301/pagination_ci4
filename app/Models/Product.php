<?php

namespace App\Models;

use CodeIgniter\Model;

class Product extends Model
{
    protected $table = 'test_1_product';
    
    public function getDataList()
    {
        $this->builder()
             ->select(["{$this->table}.*", 'test_1_category.name as name_category'])
             ->join('test_1_category', "{$this->table}.category_id = test_1_category.category_id");
        return [
            'data_lists'    => $this->paginate(3),
            'pager'         => $this->pager,
        ];
    }
}
